/**
 * @author: Amr ALmgwary
 * @date: 30-Jan-17.
 */

import Request from './Request'

/**
 * this class collect All File API
 */
const File  = {





    /**
     * @name: Upload
     * @method:
     * @paramters:
     * @responceSucsses:
     * @responceFaile:
     * @link:
     * * * * */
    fileUpload:(PatientFile , File,onUploadProgress) =>{
        var subUrl = 'File/Upload' ;
        return Request.fileUpload(subUrl,PatientFile , File,onUploadProgress);
    },


    //[HttpPost]
    GetPatientPhoto: (PatientID)=>
    {
        var subUrl = 'File/GetPhoto' + '?id='+PatientID ;
        return Request.get(subUrl);
    },



    //[HttpPost]
    GetPatientDocument: (PatientID)=>
    {
        var subUrl = 'File/GetDocument'+ '?id='+PatientID ; ;
        return Request.get(subUrl);
    },



    //[HttpPost]
    GetPatientLabResult: (PatientID)=>
    {
        var subUrl = 'File/GetLabResult'+ '?id='+PatientID ; ;
        return Request.get(subUrl);
    },



    //[HttpPost]
    GetPatientXRay: (PatientID)=>
    {
        var subUrl = 'File/GetXRay' + '?id='+PatientID ;;
        return Request.get(subUrl);
    },


};

export default File ;


/*
 console.log('upload file',);
 debugger ;


 var config = {

 onUploadProgress: function(progressEvent) {
 var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
 console.log('upload file',percentCompleted , '%');
 }
 };

 const data = new FormData();
 for (var PatientFileKey in PatientFile) {
 data.append(PatientFileKey, PatientFile[PatientFileKey]);
 }
 data.append('file', File);
 axios.post('http://localhost/DNAAPI/api/File/Upload',data,config)
 .then(function (res) {
 console.log('upload file res',res );
 })
 .catch(function (err) {
 console.log('upload file err',err );
 });

 */