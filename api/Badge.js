/**
 * @author: Amr ALmgwary
 * @date: 29-Jan-17.
 */


import Request from './Request'
/**
 * this class collect All Patient API
 */
const Badge  = {

    /**
     * @name: GetBadge
     * @method: GET
     * @paramters: BadgeID:int
     * @responceSucsses: Badge[]
     * @responceFaile: error:string
     * @link: go.le/GetBadge
     * * * * */
    GetAll:(BadgeID) =>{
        var subUrl = 'Badge/GetAll' ;
        return Request.get(subUrl);
    },




    /**
     * @name: GetAll
     * @method: GET
     * @paramters:
     * @responceSucsses: Badge[]
     * @responceFaile: error:string
     * @link:
     * * * * */
    GetAll:() =>{
        var subUrl = 'Badge/GetAll' ;
        return Request.get(subUrl);
    },


    //Badge/GetByID(long id)
    // example : localhost/DNAAPI/api/Badge/GetByID?id=5
};

export default Badge ;