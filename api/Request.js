/**
 * @author: Amr ALmgwary
 * @date: 29-Jan-17.
 */
import axios from 'axios'
import Constants from '../constants/Constants'
/**
 * this class make all API requests operations
 * this class act as middleware
 * */




const Request = {


    /**
     * onUploadProgress is used to watch file upload
     *       onUploadProgress: function(progressEvent) {
     *        var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
     *      }
     * */
    // make post request
    post: (subUrl,data)=>{
        let method = 'POST';
        return RequestHandler.handelRequest(subUrl,data,method);
    },


    // make post request
    // subUrl:string,parameters:any[]
    // return Promise<any>
    get:(subUrl,parameters)=>{
        let method = 'GET';
        //map parameters to subUrl
        return RequestHandler.handelRequest(subUrl,parameters,method);
   },

    // Warning FileUpload is Form object should not be nested
    fileUpload:(subUrl, data, File,onUploadProgress)=>{


        let method = 'POST';

        // prepare form data
        const formData = new FormData();
        for (var dataKey in data) {
            formData.append(dataKey, data[dataKey]);
        }
        formData.append('file', File);

        // add onUploadProgress to config
        var config = {onUploadProgress }  ;

        // make the request
        return RequestHandler.handelRequest(subUrl,formData, method ,config)

    }



}

// Here is the middle ware
// subUrl:string,data,method
const RequestHandler = {
    handelRequest : (subUrl,data,method,config) => {

        var url = Constants.Global.Dev_URL  + subUrl ;


        return  new Promise((resolve, reject) => {
            // make request

            axios({ method,url,data,config})
            .then(function (responce) {

                    resolve(responce)
            })
            .catch(function (error) {
                    reject(error)
            });
            // disable middleware
            /* resolve(this.handleResponse);
            // or catch
            reject(this.handleError);*/
        });



    },
    handleResponse : (response) => {return response},

    handleError : (error) => {
        // case Unauthorized
        if (error.statusCode == 401 ) {

        } else {

        }

        return error
    },
}

export default Request ;