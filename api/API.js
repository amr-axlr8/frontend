import Patient  from  './Patient'
import Badge  from  './Badge'
import Branch  from  './Branch'
import Entity  from  './Entity'
import File  from  './File'
import Group  from  './Group'
import Permission  from  './Permission'
import ReservationRequest  from  './ReservationRequest'
import ReservationRequestStatus  from  './ReservationRequestStatus'
import Room  from  './Room'
import Shift  from  './Shift'
import ShiftException  from  './ShiftException'
import Speciality  from  './Speciality'
import User  from  './User'
import Doctor  from  './User'



/**
 * this class collect All API models and classes
 */

const API =  {
    Badge,
    Branch,
    Entity,
    File,
    Group,
    Patient,
    Permission,
    ReservationRequest,
    ReservationRequestStatus,
    Room,
    Shift,
    ShiftException,
    Speciality,
    User,
    Doctor,
};

export default API ;
