/**
 * @author: Amr ALmgwary
 * @date: 29-Jan-17.
 */
import Request from './Request'
/**
 * this class collect All Patient API
 */
const Patient  = {


    /**
     * @name: AddPatient
     * @method: POST
     * @paramters: Patient:Patient
     * @responceSucsses: Patient:Patient
     * @responceFaile: error:string
     * @link: go.le/AddPatient
     * * * * */
    AddPatient : (Patient)=>{
       // var subUrl = this.PatientUrl + '/AddPatient' ;
       // return this.request.post(subUrl,Patient) ;
        console.error('No API implementation');
    },




    /**
     * @name: GetPatient
     * @method: GET
     * @paramters: PatientID:int
     * @responceSucsses: Patient:List<Patient>
     * @responceFaile: error:string
     * @link: go.le/GetPatient
     * * * * */
    GetPatient:(PatientID) =>{
        var subUrl = 'Patient/GetAll' ;
        return Request.get(subUrl);
    }



    //Patient/GetByID(long id)

    //Patient/GetByName(string Name)
    //localhost/DNAAPI/api/Patient/GetByName?Name=Shreef

    //Patient/GetByMobile(string Mobile)
    //localhost/DNAAPI/api/Patient/GetByMobile?Mobile=4654
};

export default Patient ;