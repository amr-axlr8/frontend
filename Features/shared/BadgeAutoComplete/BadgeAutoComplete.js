/**
 * @author: Amr ALmgwary
 * @date: 29-Jan-17.
 */



import React from 'react';
import { connect } from 'react-redux';
import store from '../../../store';
import Autosuggest from 'react-autosuggest';
import API from '../../../api/API'

require('./style.scss');


// ToDo: refactor loadBadges to API module
class BadgeAutoComplete extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: '',
            badges: [],
            isLoading: false,
            isOpen:false
        };

    }

    renderBadge(badge) {

        return (
            <div className="list-row">
                <span>{badge.value} </span>
            </div>
        );
    }

    // called after select item and return new value to search input
    getBadgeValue(badge) {
        console.log('getBadgeValue: ',badge);
        this.onItemSelect(badge);
        return badge.value;
    }

    onItemSelect(item){
        this.props.onItemSelect(item) ;
        this.setState({
            isOpen:false,
            badges: []
        });
    }

    // ToDo: refactor to API module
    // load badge form API
    loadBadges(value) {
        var self = this ;
        this.setState({
            isLoading: true
        });

        API.Badge.GetBadge()
            .then(function (responce) {
                self.setState({
                    isLoading: false,
                    badges: responce.data
                });
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    // auto bind search input value
    onChange (event, { newValue }) {
        console.log('onChange: ',newValue);
        this.setState({
            value: newValue
        });

    };

    // called each time search input change and not empty
    // should call API
    onBadgesFetchRequested ({ value }) {
        console.log('onBadgesFetchRequested: ',value);
        this.loadBadges(value);
    };

    // called when search input is empty
    onBadgesClearRequested ()  {
        this.setState({
            badges: []
        });
    };

    render() {


        return (
            <div className="badge-auto-complete">
                {this.state.isOpen ? this.renderOpen() : this.renderClose()}
            </div>
        );
    }


    renderOpen(){
        const { value, badges, isLoading } = this.state;
        const searchInputProps = {
            placeholder: "select Badge",
            value,
            onChange: this.onChange.bind(this)
        };
        const status = (isLoading ? 'Loading...' : '');
        return(
            <div>
                <div className="status">
                    {status}
                </div>
                <Autosuggest
                    suggestions={badges}
                    onSuggestionsFetchRequested={this.onBadgesFetchRequested.bind(this)}
                    onSuggestionsClearRequested={this.onBadgesClearRequested.bind(this)}
                    getSuggestionValue={this.getBadgeValue.bind(this)}
                    renderSuggestion={this.renderBadge.bind(this)}
                    inputProps={searchInputProps} />
                <br/>

                    <button onClick={()=> {this.onCancelClick()}}>cancel</button>
            </div>

        )
    }

    renderClose(){
        return(
            <div onClick={()=> {this.onOpenClick()}}>
                <i className="fa fa-plus" aria-hidden="true" ></i>
                Add Badge
            </div>
        )

    }

    onOpenClick(){
        this.setState({isOpen:true});
    }
    onCancelClick(){
        this.setState({isOpen:false});
    }
}



const mapStateToProps = function(store) {
    return {};
};

export default connect(mapStateToProps)(BadgeAutoComplete);