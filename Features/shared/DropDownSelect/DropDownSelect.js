/**
 * @author: Amr ALmgwary
 * @date: 26-Jan-17.
 */



import React from 'react';
import { connect } from 'react-redux';
import store from '../../../store';

require('./style.scss');

/**
 * Popup DropDownSelect
 * Usage: render list of item and make callback onItemSelect
 * Props:items:object[], selectedItem:object, onItemSelect:func
 * State:isOpen:bool, selectedItem:object
 * Callback:onItemSelect()
 * TODO :
 * */


var DropDownSelect =  React.createClass({

    propTypes: {
        items: React.PropTypes.array.isRequired,
        selectedItem: React.PropTypes.object,
        onItemSelect: React.PropTypes.func
    },

    getInitialState: function() {
        return {
            isOpen: false,
            selectedItem:this.props.selectedItem
        }
    },


    onBlur:()=>{
      this.setState({isOpen:false});
    },


    // handle Popup close
    onClose: function(){
        console.log('DropDownSelect onClose');
        this.setState({isOpen: false});
    },


    /**
     * render each item
     * add class to selected item
     * callback onItemSelect
     * */
    renderItems() {
        var self = this ;
        return this.props.items.map( (item, index) => {
            var  isSelectedClass =   self.state.selectedItem === item ?   'selected':'';
            return (
                <li key={index}
                    className={'option ' + isSelectedClass}
                    onClick={(event)=> self.onItemSelect(event,index ,item)}>{item.Value}</li>
            )
        })
    },



    /**
     * onItemSelect
     * make callback and update current component state
     * */
    onItemSelect(event,itemIndex,item) {
        console.log('selectItem',item);
        this.setState({isOpen: false,selectedItem:item});
        this.props.onItemSelect(item)  ;
    },

    /**
     * toggle component isOpen
     */
    toggleDropDownList(){
        this.setState({isOpen: !this.state.isOpen});
    },


    render:function() {

        // handel open, close list
        const isOpenClasses =  this.state.isOpen?  ' open': '' ;
        return (
            <div className="drop-down-select">

                    <div className="header" onClick={this.toggleDropDownList}>
                       <span className="text">  {this.state.selectedItem ? this.state.selectedItem.Value : this.props.placeholder || 'select'} </span>
                        <i className="fa fa-caret-down" aria-hidden="true"></i>
                    </div>
                    <ul className={'options ' + isOpenClasses}>
                        {this.renderItems()}
                    </ul>

            </div>
        )
    }
});


const mapStateToProps = function(store) {
    return {};
};

export default connect(mapStateToProps)(DropDownSelect);