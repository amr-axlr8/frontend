/**
 * @author: Amr ALmgwary
 * @date: 26-Jan-17.
 */



import React from 'react';
import { connect } from 'react-redux';
import store from '../../../store';
require('./style.scss');


/**
* SmartList Component
* Usage: used to render  array of items
* Callback: onItemClick(), onRemoveItemClick()
* Props: items[]:Array<any> ,renderItem(item,index)
* example: <SmartList
*                items={this.state.items}
*                onItemClick={this.method}
*                onRemoveItemClick={this.method}
*                />
* ToDo: customize item render values, show only list of item properties
* */

var SmartList = React.createClass({

    getInitialState : function(){
      var state =  {}
      return  state ;
    },

    onItemClick :function(event, item){
        console.log('SmartList onItemClick');
        event.stopPropagation();
        this.props.onItemClick(item);
    },


    onRemoveItemClick :function(event, item){
        console.log('SmartList onRemoveItemClick');
        event.stopPropagation();
        this.props.onRemoveItemClick(item);

    },


    render:function(){
        var self = this;
        var classes =  "smart-list";


        var isEmpty = !(this.items  && this.items.length > 0);
        if(isEmpty) classes+= " empty"

            return(
                <div className={classes}>
                    { this.renderItems()}
                </div>
            )


    },


    renderItem:function(item, index){
        // check custom item render
        if(this.props.renderItem){
           return this.props.renderItem(item, index);
        }
        else
        return(
            <span>{item.Value}</span>
        )
    },
    renderItems:function(){
        var self = this;
        //check empty classes
        var isEmpty = false;

        if(!this.props.items || this.props.items.length == 0){
            isEmpty = true ;
        }
        return (
            <ul className={  isEmpty?"list-items  is-empty":"list-items" }>
                {this.props.items.map((item, index) => {
                    return(
                        <li className="item"
                            key={index}
                            onClick={(event) =>  self.onItemClick(event, item)} >

                            {self.renderItem(item, index)}
                            <span className="label label-danger"  onClick={(event) => self.onRemoveItemClick(event, item)}>
                                <i className="fa fa-minus" aria-hidden="true"></i>

                            </span>
                        </li>
                    )
                })}
            </ul>
        )
    },
});

export default SmartList;