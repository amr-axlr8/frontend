/**
 * @author: Amr ALmgwary
 * @date: 24-Jan-17.
 */

import React from 'react';
import { connect } from 'react-redux';
import store from '../../../store';
import TabContents from './TabContents';
import TabMenu from './TabMenu'
require('./style.scss');


/**
 * Tabs Component
 * Usage: used to render custome array of tabs each of type {title:string , content:component}
 * Props: contents:Array[],
 * State: activeTapIndex:int,
 * Callback:
 * example:  <Tabs contents={myArray}/>
 *           where myArray = [{ tab_title: 'Tab 1', tab_contents: '<div> this is content </div>}, .... ]
 * */

var Tabs = React.createClass({


    getInitialState: function() {
        return {
            activeTapIndex: 0
        }
    },


    setActiveTab: function(index) {
        this.setState({
            activeTapIndex : index
        });
    },



    // render Tabs list
    renderMenus: function(){
        var self = this;

        var menus = this.props.contents.map(function(item, i) {
            return <TabMenu key={i} setActiveTab={self.setActiveTab} title={item.tab_title} myIndex={i} activeTapIndex={self.state.activeTapIndex} />
        });
        return menus ;
    },






    // render each tab content
    // each TabContents hide it self if it is not selected
    renderContents: function(){
        var self = this;
        var contents = this.props.contents.map(function(item, i){
            return <TabContents key={i} contents={item.tab_contents} myIndex={i} activeTapIndex={self.state.activeTapIndex} />
        });
        return contents ;
    },



    render: function() {
        return (
            <div className="tabs-container">
                <div className="tabs">
                    <div className="tab-menus">{this.renderMenus()}</div>
                    <div className="tab-contents">{this.renderContents()}</div>
                </div>
            </div>
        )
    }
});


export default Tabs;