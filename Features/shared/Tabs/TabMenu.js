/**
 * @author: Amr ALmgwary
 * @date: 24-Jan-17.
 */

import React from 'react';
import { connect } from 'react-redux';
import store from '../../../store';

/**
 * TabMenu Component
 * Usage: used to render TabMenu and controll tap selection
 * */
var TabMenu = React.createClass({

    changeTab: function() {
        this.props.setActiveTab(this.props.myIndex);
    },

    render: function() {
        var classes = 'tab-menu ';

        if(this.props.myIndex == this.props.activeTapIndex) {
            classes += 'active';
        }

        return (
            <a href="#" onClick={this.changeTab} className={classes}>{this.props.title}</a>
        )
    }
});

export default TabMenu ;
