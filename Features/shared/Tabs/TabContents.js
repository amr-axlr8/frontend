/**
 * @author: Amr ALmgwary
 * @date: 24-Jan-17.
 */

import React from 'react';
import { connect } from 'react-redux';
import store from '../../../store';


/**
 * TabContents Component
 * Usage: used to render selected tap content
 * */


var TabContents = React.createClass({
    render: function() {
        var classes = 'tab-contents-wrapper ';

        if(this.props.myIndex != this.props.activeTapIndex) {
            classes += 'hidden';
        }

        return (
            <div className={classes} >
                {this.props.contents}
            </div>
        )
    }
});

export default TabContents ;
