/**
 * @author: Amr ALmgwary
 * @date: 31-Jan-17.
 */

import BadgeAutoComplete from'./BadgeAutoComplete/BadgeAutoComplete'
import DropDownSelect from'./DropDownSelect/DropDownSelect'
import PatientFileForm from'./PatientFileForm/PatientFileForm'
import Popup from'./Popup/index'
import SmartInput from'./SmartInput/SmartInput'
import Tabs from'./Tabs/Tabs'
/**
 * this file collect all shared components
 * */

const SharedComponents =  {
    BadgeAutoComplete,
    DropDownSelect,
    PatientFileForm,
    Popup,
    SmartInput,
    Tabs,
};

export default SharedComponents ;
