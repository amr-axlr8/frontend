/**
 * @author: Amr ALmgwary
 * @date: 31-Jan-17.
 */

import React from 'react';
import { connect } from 'react-redux';
import store from '../../../store';
import axios from 'axios'
require('./style.scss');

/**
 * PatientFileForm Component
 * Usage:
 * Props:
 * State:
 * Callback:
 * TODO :
 * */


var PatientFileForm =  React.createClass({


    getInitialState: function() {
        return {
            PatientFile:{
                PatientFileID:0,
                Name:"",
                Description:"",
                Note:"",
                Guid:"",
                FileTypeID:this.props.fileType || "",
                PatientID:1,
            },
            File:null
        }
    },
    // handle Popup close
    onSave: function(){
        console.log('Popup onClose');

    },

    updateState:function(event,type){

        var newState =  Object.assign({}, this.state);
        if(type == "Name"){
            newState.PatientFile.Name = event.target.value ;
        } else if(type == "Description"){
            newState.PatientFile.Description = event.target.value ;
        } else if(type == "Note"){
            newState.PatientFile.Note = event.target.value ;
        }else if(type == "File"){
            newState.File= event.target.files[0];
        }
        this.setState(newState);
    },


    onSaveClick:function(){
        var {PatientFile , File} = this.state ;
       this.props.onSaveClick(PatientFile , File);
    },
    render:function() {
        var {fileType} = this.props ;


        return (
            <div className="patient-file-form">
                 <div className="left col-50">


                <div className="formRow">
                    <label className="formLabel">File</label>
                    <input placeholder="Name md-button" type="file" onChange={(event) => {this.updateState(event,"File")}}/>
                </div>

                <div className="formRow">
                    <label className="formLabel">Name</label>
                    <input placeholder="Name"  value={this.state.PatientFile.Name} onChange={(event) => {this.updateState(event,"Name")}}/>
                </div>


                <div className="formRow">
                    <label className="formLabel">Description</label>
                    <input placeholder="Description"  value={this.state.PatientFile.Description} onChange={(event) => {this.updateState(event,"Description")}}/>
                </div>


                <div className="formRow">
                    <label className="formLabel">Note</label>
                    <input placeholder="Note"  value={this.state.PatientFile.Note} onChange={(event) => {this.updateState(event,"Note")}}/>
                </div>



                 </div>
                <div className="right col-50">
                    <span className="upload" onClick={(event)=> {this.onSaveClick(event);}}>
                        <i className="fa fa-cloud-upload" aria-hidden="true"></i>
                        <span className="text">
                            Save
                        </span>
                    </span>
                </div>
            </div>
        )
    }
});


const mapStateToProps = function(store) {
    return {};
};

export default connect(mapStateToProps)(PatientFileForm);