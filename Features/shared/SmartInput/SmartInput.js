/**
 * @author: Amr ALmgwary
 * @date: 26-Jan-17.
 */






import React from 'react';
import { connect } from 'react-redux';
import store from '../../../store';
require('./style.scss');


/**
 * SmartInput Component
 * Usage: used to render  array of items
 * Callback: onSaveClick(),
 * Props: value:inputValue, type:inputType, isOpen:bool, closeMessage:string
 * example: <SmartInput
 *                type={'text'}
 *                value={''}
 *                isOpen={false}
 *                closeMessage={'Add More'}
 *                onSaveClick={this.method}
 *                />
 * ToDo: customize item render values, show only list of item properties
 * ToDo: change state to props and handel changes
 * */

var SmartInput = React.createClass({

    getInitialState : function(){
        var state =  {
                        value:this.props.value || '',
                        newValue:this.props.value || '',
                        type:this.props.type || 'text',
                        isOpen:this.props.isOpen || false,
                        closeMessage:this.props.closeMessage|| 'Add More'
        }
        return  state ;
    },

    /*callback parent with new value*/
    onSaveClick :function(event){
        console.log('SmartInput onSaveClick');
        event.stopPropagation();
        var {newValue,value} = this.state ;
        this.setState({isOpen:false,newValue:value});
        this.props.onSaveClick(newValue);
    },

    /* handel cansle and reset input to initial value*/
    onCancelClick :function(event){
        console.log('SmartInput onCancelClick');
        event.stopPropagation();
        var {value} = this.state ;
        this.setState({newValue:value,isOpen:false});
    },

    /* handel newValue changes*/
    handelInputChange:function(event){
      this.setState({newValue:event.target.value});
    },

    onOpenClick:function(){
        console.log('onOpenClick');
        this.setState({isOpen:true});
    },

    render:function(){
        var{isOpen} = this.state ;

        return(
            <div className="smart-input">
                {isOpen? this.renderOnOpen():this.renderOnClose() }
            </div>
        )
    },
    renderOnOpen:function(){
        var{type, newValue } = this.state ;
        return (
            <div className="open">
                <input
                    type={type}
                    value={newValue}
                    onChange={(event)=>{this.handelInputChange(event)}} />
                <button onClick={(event)=> this.onSaveClick(event)}> save</button>
                <button onClick={(event)=> this.onCancelClick(event)}> cancel</button>
            </div>

        )
    },

    renderOnClose:function(){
        var{ closeMessage } = this.state ;
        return (
            <div className="close" onClick={(event)=>{this.onOpenClick(event)}}>
                <div>  <i className="fa fa-plus" aria-hidden="true"></i> {closeMessage}</div>
            </div>

        )
    },
});

export default SmartInput;