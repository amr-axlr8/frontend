/**
 * @author: Amr ALmgwary
 */
import React from 'react';
import { connect } from 'react-redux';
import store from '../../../store';

require('./style.scss');

/**
 * Popup Component
 * Usage: used to render children component in popup window
 * Props: title:string,
 * State: isOpen:bool,
 * Callback: onClose(),
 * TODO : add close button , it can disable 'popup-show' class and make onClose callback
 * */


var Popup =  React.createClass({


    getInitialState: function() {
        return {
            isOpen: true
        }
    },
    // handle Popup close
    onClose: function(){
        console.log('Popup onClose');
        this.setState({isOpen: false});
    },


    render:function() {
        var {title} = this.props ;
        // handel show class
        var showPopUpClass = this.state.isOpen ? "popup-show" : ""

        return (
            <div className="popup">
                {/* background shadow and fixed container */}
                <div   className={"popup-overlay " + showPopUpClass} >
                    {/* page content */}
                    <div className="popup-content">
                        {/* close button */}
                        <a className="popup-close" onClick={this.onClose.bind(this)}>&times;</a>
                        {/* render Title  */}
                        <div className="popup-title">{title}</div>
                        {/* render children  */}
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
});


const mapStateToProps = function(store) {
    return {};
};

export default connect(mapStateToProps)(Popup);