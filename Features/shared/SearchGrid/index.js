import React from 'react';
import ReactDOM from 'react-dom';
import SearchInput, { createFilter } from 'react-search-input';
var Griddle = require('griddle-react');
import { connect } from 'react-redux';
import { createStore, combineReducers, Redux } from 'redux';
import store from '../../../store';
require('./style.scss');
class SearchGrid extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //initial data from parent
            externalSortColumn : this.props.sortBy,
            currentPage:0,
            maxPages:0  

        }
    }
    componentDidMount() {
        this.search()
    }
    setValues(page) {
        const data = this.props.patients.data;
        let count = this.props.patients.count;
        let maxPages = Math.round(count / this.props.pageSize);
        if (maxPages == 0) { maxPages = 1 }
        this.setState({ data, maxPages, currentPage: page - 1 });
    }
    search(page) {
    
        var self = this;//call back function change context
        this.props.fetchData(page, this, function (page) {
            //fetch data from parent component
            self.setValues(page);
        });
    }
    searchUpdated(type, term) {
        this.setState({ [type]: term });
        this.search();
    }
    setPage(index) {
        index = index > this.state.maxPages ? this.state.maxPages : index < 1 ? 1 : index + 1;
        this.search(index);
    }
    changeSort(sort, sortAscending) {
        sortAscending === true ? sortAscending = "ase" : sortAscending = "des";
        this.setState({ externalSortColumn: sort, externalSortAscending: sortAscending }, function () {
            this.search();
        })
    }
    renderGrid() {
        return (
            <div>
                <Griddle 
                    showFilter={true}
                    showSettings={true} 
                    results={this.props.patients.data}
                    tableClassName="table"
                    />
            </div>
        );
    }
    renderFilters() {
        return (
            //render filters coming from parent component
            <div>
                {this.props.filters.map((filter, index) =>
                    <SearchInput key={index} placeholder={filter.placeholder} className="search-input" onChange={this.searchUpdated.bind(this, filter.name)} />
                )}
            </div>
        )
    }
    render() {
        if (this.props.patients) {
            return (
                <div>
                    {this.renderGrid()}
                </div>
            )
        } else {
            return (
                <div>
                    <div className="loader"></div>
                </div>
            )
        }
    }
}

const mapStateToProps = function(store) {
  return {
    patients: store.patientState.patients
  };
}

export default connect(mapStateToProps)(SearchGrid);