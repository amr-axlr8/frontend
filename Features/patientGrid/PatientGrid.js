import React from 'react';
import {connect} from 'react-redux';
import store from '../../store';
import * as PatientActions  from '../../ducks/actions/patient.actions'

class PatientGrid extends React.Component {



    onAddPatientClick(event,data){
        console.log('Handel click',this.props);
        this.props.addPatient({name:'c'});
    }
    render() {
        return (
            <div>
               This Patient Grid
                <br/>
                {JSON.stringify(this.props.patients)}
                <br/>
                <button onClick={(event) => {this.onAddPatientClick(event,1)}}>Add</button>
            </div>
        )
    }
}



/**
 * mapStateToProps that tells how to transform the current Redux store state into the props you want to pass to a presentational component you are wrapping.
 * */
const mapStateToProps = function(state) {
  return {
    patients: state.patientState.patients
  };
};


/**
 * container components can dispatch actions. You can define a function called mapDispatchToProps() that receives the dispatch() method and returns callback props that you want to inject
 * into the presentational component.
 * */
const mapDispatchToProps = function(dispatch){

    return {
        addPatient: (patient) => {
            dispatch(PatientActions.addPatient(patient))
        }
    }
};

/**
 * connect is make our component subscript to state and map states and action to props in our component
 * */
export default connect(mapStateToProps,  mapDispatchToProps)(PatientGrid);
