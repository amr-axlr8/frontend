import  {createStore}  from 'redux';
import reducers from './ducks/reducers/reducers';


/** * * * * * * * * * * * * * * * * *
 *             Store
 * * * * * * * * * * * * * * * * * *
 * Holds application state;
 * Allows access to state via getState();
 * Allows state to be updated via dispatch(action);
 * Registers listeners via subscribe(listener);
 * Handles unregistering of listeners via the function returned by subscribe(listener)
 *
 * It's important to note that you'll only have a single store in a Redux application. When you want to split your data handling logic, you'll use reducer composition instead of many stores.
 *
 */
const store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
export default store;