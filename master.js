import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router';
import patientGrid from './features/PatientGrid/PatientGrid'

require('./style.scss');
import { SideMenu, Item } from 'react-sidemenu';

class master extends React.Component {
  getComponent(value) {
    browserHistory.push(`${value}`)
  }

  render() {
    return (
      <div>
        <header className="MainHeader"><i className="fa fa-heartbeat"></i><span>DNA clinic</span></header>
        <SideMenu onMenuItemClick={(value) => this.getComponent(value)} >
          <Item divider={true} label="Calendar" value="/Main" />
          <Item label="patients" value="/patients" icon="fa fa-stethoscope" />
          {/*
           <Item label="calendar" value="/calendar" icon="fa-calendar">
           { <Item label="Item 1.1.2" value="item1.1.2" icon="fa-bar-chart" />
           <Item label="Item 1.2" value="item1.2" />}
            </Item>
            */}
          <Item label="Add Patient" value="/addPatient" icon="fa-user-plus">
          </Item>
          <Item divider={true} label="Segment 2" value="segment2" />
          <Item label="Create Reservation" value="/CreateReservation" icon="fa fa-plus" />



        </SideMenu>

        <div id="content">
          <div className="pageContent">
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
}
export default master;