import {combineReducers}  from 'redux';
import PatientReducer from './patient.reducer' ;

/**
 * Reducers
 * specify how the application's state changes
 */

/***
 * we can rewrite the main reducer as a function that calls the reducers managing parts of the state,
 * and combines them into a single object. It also doesn't need to know the complete initial state anymore.
 * It's enough that the child reducers return their initial state when given undefined at first.
 * Note that each of these reducers is managing its own part of the global state.
 * The state parameter is different for every reducer, and corresponds to the part of the state it manages.
 */

  /**
   * How combine reducer work
   *  -----------------------
   *  All combineReducers() does is generate a function that calls your reducers with the slices of state selected according to their keys, and combining their results into a single object again.
   */

const reducers = combineReducers({
    patientState: PatientReducer
});

export default reducers;
