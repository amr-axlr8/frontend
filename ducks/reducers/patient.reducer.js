import * as PatientAction from '../actions/patient.actions';

/**
 * Reducers
 * specify how the application's state changes
 */

const initialState = {
    patients: [{name:'ali'}]

};


/***
 * We don't mutate the state. We create a copy with Object.assign().
 * Object.assign(state, { visibilityFilter: action.filter }) is also wrong: it will mutate the first argument. You must supply an empty object as the first parameter.
 * You can also enable the object spread operator proposal to write { ...state, ...newState } instead.
 * We return the previous state in the default case. It's important to return the previous state for any unknown action
 */


// set initialState if state is empty [ state = initialState  ] es6 code
// which equal to
// if (typeof state === 'undefined') {
//    return initialState
//}
const PatientReducer = function (state = initialState , action) {



    switch (action.type) {
        case PatientAction.ADD_PATIENT:
            return Object.assign({}, state, { patients: [...state.patients,action.patient]});
    }

    return state;
};

export default PatientReducer;