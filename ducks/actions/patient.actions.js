/***
 * Actions are payloads of information that send data from your application to your store.
 * They are the only source of information for the store.
 * You send them to the store using store.dispatch().
 */


/*
 * action types
 */
export const ADD_PATIENT = 'ADD_PATIENT'
export const PatientFilters  = {
    SHOW_ALL: 'SHOW_ALL',
    SHOW_COMPLETED: 'SHOW_COMPLETED',
    SHOW_ACTIVE: 'SHOW_ACTIVE'
}

/*
 * Action creators
 * Action creators are exactly that—functions that create actions.
 */
export function addPatient(patient){
    return {
        type: ADD_PATIENT,
        patient : patient
    }
}

