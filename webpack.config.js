var ExtractTextPlugin = require("extract-text-webpack-plugin");
module.exports = {
    entry: './app.js',
    module: {
        loaders: [
            {
                exclude: /node_modules/,
                loader: 'babel-loader?presets[]=es2015&presets[]=react'
            },
     { 
      test: /\.scss$/, 
      loader: ExtractTextPlugin.extract("style", "css!sass","scss") 
  },
          {
          test: /\.s?css$/,
          include: /node_modules/,
          loaders: ['style-loader','css-loader', 'sass-loader']
        }

        ]
    },
    output: {
        filename: './dist/bundle.js'
    },
    devServer: {
        historyApiFallback: true,
        port: 3030
    },
    plugins: [
        new ExtractTextPlugin("./dist/styles.css", {
        allChunks: true
      })
    ]
};