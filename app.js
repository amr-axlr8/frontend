import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router';
import PatientGrid from './Features/patientGrid/PatientGrid'

require('./style.scss');
import { Provider } from 'react-redux';
import {routes} from './router';
import store from './store';


class app extends React.Component {
  render() {
    return (
      <div>
        <aside>
          <h1>Meneu</h1>
          <ul className="header">
            <li><Link to="/select">select</Link></li>
            <li><Link to="/patients">grid</Link></li>
            <li><Link to="/calendar">calendar</Link></li>
            <li><Link to="/redux">redux test</Link></li>
          </ul>
        </aside>
        <div id="content">
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default app;
ReactDOM.render(
    <Provider store={store}>{routes}</Provider>,
    document.getElementById('app')
);