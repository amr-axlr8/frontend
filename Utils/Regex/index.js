var isPhone = function(string) {
return /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(string)
}
var isName = function(string) {
return /^[a-zA-Z ]+$/.test(string)
}


exports.isPhone = isPhone;
exports.isName = isName;