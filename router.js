import React from 'react';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import PatientGrid from './features/PatientGrid/PatientGrid'

import master from './master'
export const routes = (
    <Router history={browserHistory}>
        <Route component={master}>
            <Route path="/" component={PatientGrid} />
            <Route path="patients" component={PatientGrid} myprop="foo" />
        </Route>
    </Router>
);