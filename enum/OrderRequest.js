/**
 * @author: Amr ALmgwary
 * @date: 02-Feb-17.
 */


/**
 * this file must mapped to DNA utils enums EnumOrderRequest
 * */


const  OrderRequest = {
    Add : 1,
    Change : 2,
    Cancel : 3,
}

export default OrderRequest ;