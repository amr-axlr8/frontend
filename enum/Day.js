/**
 * @author: Amr ALmgwary
 * @date: 30-Jan-17.
 */

/**
 * this file must mapped to DNA utils enums EnumDay
 * */



const  Day = {
    Sunday: 0,
    Monday: 1,
    Tuesday : 2,
    Wednesday : 3,
    Thursday : 4,
    Friday : 5,
    Saturday : 6
}

export default Day ;