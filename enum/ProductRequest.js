/**
 * @author: Amr ALmgwary
 * @date: 02-Feb-17.
 */


/**
 * this file must mapped to DNA utils enums EnumProductRequest
 * */


const  ProductRequest = {
    Add : 1,
    Change : 2,
    Remove : 3,
    NoChange : 4,
}

export default ProductRequest ;