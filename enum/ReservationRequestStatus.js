/**
 * @author: Amr ALmgwary
 * @date: 30-Jan-17.
 */

/**
 * this file must mapped to DNA utils enums EnumReservationRequestStatus
 * */



const  ReservationRequestStatus = {
    New : 1,
    InProgress : 2,
    PendingOnApproval : 3,
    Completed : 4,
}

export default ReservationRequestStatus ;