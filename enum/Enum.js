/**
 * @author: Amr ALmgwary
 * @date: 02-Feb-17.
 */
import Day from './Day'
import FamilyMemberType from './FamilyMemberType'
import ReservationRequestStatus from './ReservationRequestStatus'
import ReservationTypeStatus from './ReservationTypeStatus'
import Group from './Group'
import FileType from './FileType'
import Status from './Status'
import ProductRequest from './ProductRequest'
import OrderRequest from './OrderRequest'

/**
 * this class export all system constants
 */
const Constants  = {
    Day,
    FamilyMemberType,
    ReservationRequestStatus,
    ReservationTypeStatus,
    Group,
    FileType,
    Status,
    ProductRequest,
    OrderRequest
};

export default Constants;

