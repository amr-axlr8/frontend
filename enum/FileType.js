/**
 * @author: Amr ALmgwary
 * @date: 31-Jan-17.
 */


/**
 * this file must mapped to DNA utils enums EnumFileType
 * */



const  FileType = {
        Photo :1,
        Document :2,
        LabResult :3,
        XRay :4
};

export default FileType ;