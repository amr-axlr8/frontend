/**
 * @author: Amr ALmgwary
 * @date: 02-Feb-17.
 */


/**
 * this file must mapped to DNA utils enums EnumStatus
 * */


const  Status = {
    Active : 1,
    PendingOnApproval : 2,
    Completed : 3,
    Canceled : 4,
    PendingOnPatientConfirmation : 5,
}

export default Status ;