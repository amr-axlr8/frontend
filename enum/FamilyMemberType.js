/**
 * @author: Amr ALmgwary
 * @date: 30-Jan-17.
 */

/**
 * this file must mapped to DNA utils enums EnumFamilyMemberType
 * */



const  FamilyMemberType = {
    Father : 1,
    Mother : 2,
    Sister : 3,
    Brother : 4,
    Son : 5,
    Daughter : 6,
    Grandfather : 7,
    Grandmother : 8,
    Grandson : 9,
    Granddaughter : 10,
    Uncle : 11,
    Aunt : 12,
    Nephew : 13,
    Niece : 14,
    Wife : 15,
    Husband : 16,
}

export default FamilyMemberType ;