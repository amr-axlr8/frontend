import BigCalendar from 'react-big-calendar';
import React from 'react';
import moment from 'moment';
import events from './events';
import Datetime from 'react-datetime';
require('./style.scss');
import Popup from 'react-popup';
// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
BigCalendar.momentLocalizer(moment); // or globalizeLocalizer
class Calendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      start: 0,
      end: 0
    }
  }
  add(start, end) {
    this.setState({ start, end })
    this.openForm()
  }
  addEvent() {
  
    console.log(this.state.start.toLocaleString())
    console.log(this.state.end.toLocaleString())
    console.log(this.refs.eventTitle.value)

  }
  openForm() {
    return (
      <div className="form full float">
        <h3>add new appointment</h3>
        <div className="float">
          <div>start Date= {this.state.start.toLocaleString()}</div>
          <Datetime input={false} defaultValue={this.state.start.toLocaleString()} onChange={this.startdateChanged.bind(this)} />
        </div>
        <div className="float">
          <div>end Date= {this.state.end.toLocaleString()}</div>
          <Datetime input={false} onChange={this.enddateChanged.bind(this)} />
        </div>
        <div className="float">
          <input ref="eventTitle" placeholder="title" />
          <button onClick={this.addEvent.bind(this)}>add</button>
        </div>
      </div>
    )
  }
  startdateChanged(val) {
    this.setState({ start: val._d })
  }
  enddateChanged(val) {
    this.setState({ end: val._d })
  }
  render() {
    return (
      <div>
       <Popup />
        {this.openForm()}
        <div className="float full">
          <BigCalendar
            selectable
            {...this.props}
            events={events}
            defaultDate={new Date(2015, 3, 1)}
            onSelectEvent={event => alert(event.title)}
            onSelectSlot={(slotInfo) => this.add(slotInfo.start, slotInfo.end)}
            />
        </div>
      </div>
    )
  }


}
export default Calendar;