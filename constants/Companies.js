/**
 * @author: Amr ALmgwary
 * @date: 29-Jan-17.
 */

const  Companies = [
    {
        "CompanyId": 1,
        "Value": "Company 1"
    },
    {
        "CompanyId": 2,
        "Value": "Company 2"
    },
    {
        "CompanyId": 3,
        "Value": "Company 3"
    }
];

export default Companies ;