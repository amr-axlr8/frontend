/**
 * @author: Amr ALmgwary
 * @date: 29-Jan-17.
 */

const Global = {
    Dev_URL: "http://localhost/DNAAPI/api/" ,
    Publish_URL: "localhost:5050/api2" ,
     contentType: "application/json",
     auth_token:"THIS_AUTH_TOKEN_IS_EMPTY",
}

export default Global ;