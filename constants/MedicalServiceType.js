/**
 * @author: Amr ALmgwary
 * @date: 29-Jan-17.
 */

const  MedicalServiceType = [
    {
        "Value": "therapeutic"
    },
    {
        "Value": "preventative"
    }
];

export default MedicalServiceType ;