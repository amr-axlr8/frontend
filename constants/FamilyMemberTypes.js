/**
 * @author: Amr ALmgwary
 * @date: 29-Jan-17.
 */

const  FamilyMemberTypes = [
    {
        "FamilyMemberTypeId": 1,
        "Value": "Father"
    },
    {
        "FamilyMemberTypeId": 2,
        "Value": "Mother"
    },
    {
        "FamilyMemberTypeId": 3,
        "Value": "Sister"
    },
    {
        "FamilyMemberTypeId": 4,
        "Value": "Brother"
    },
    {
        "FamilyMemberTypeId": 5,
        "Value": "Son"
    },
    {
        "FamilyMemberTypeId": 6,
        "Value": "Daughter"
    },
    {
        "FamilyMemberTypeId": 7,
        "Value": "Grandfather"
    },
    {
        "FamilyMemberTypeId": 8,
        "Value": "Grandmother"
    },
    {
        "FamilyMemberTypeId": 9,
        "Value": "Grandson"
    },
    {
        "FamilyMemberTypeId": 10,
        "Value": "Granddaughter"
    },
    {
        "FamilyMemberTypeId": 11,
        "Value": "Uncle"
    },
    {
        "FamilyMemberTypeId": 12,
        "Value": "Aunt"
    },
    {
        "FamilyMemberTypeId": 13,
        "Value": "Nephew"
    },
    {
        "FamilyMemberTypeId": 14,
        "Value": "Niece"
    },
    {
        "FamilyMemberTypeId": 15,
        "Value": "Wife"
    },
    {
        "FamilyMemberTypeId": 16,
        "Value": "Husband"
    }
];

export default FamilyMemberTypes ;