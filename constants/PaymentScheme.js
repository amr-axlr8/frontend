/**
 * @author: Amr ALmgwary
 * @date: 29-Jan-17.
 */

const  PaymentScheme = [
    {
        "Value": "Direct Billing"
    },
    {
        "Value": "Co Payment"
    }
];

export default PaymentScheme ;