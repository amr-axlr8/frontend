/**
 * @author: Amr ALmgwary
 * @date: 29-Jan-17.
 */

import Companies from './Companies'
import FamilyMemberTypes from './FamilyMemberTypes'
import MedicalServices from './MedicalServices'
import MedicalServiceType from './MedicalServiceType'
import PaymentScheme from './PaymentScheme'
import Global from './Global'


// deprecated : remove enums from constants
import Day from './../enum/Day'
import FamilyMemberType from './../enum/FamilyMemberType'
import ReservationRequestStatus from './../enum/ReservationRequestStatus'
import ReservationTypeStatus from './../enum/ReservationTypeStatus'
import Group from './../enum/Group'
import FileType from './../enum/FileType'



/**
 * this class export all system constants
 */
const Constants  = {
    Companies,
    FamilyMemberTypes,
    MedicalServices ,
    MedicalServiceType ,
    PaymentScheme ,
    Global,
    Day,
    FamilyMemberType,
    ReservationRequestStatus,
    ReservationTypeStatus,
    Group,
    FileType,


}



export default Constants;

